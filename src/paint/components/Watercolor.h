//
//  Watercolor.h
//  Colors
//  Contains the information of the current state of the simulation
//  Created by Joseph Chow on 1/6/16.
//
//

#ifndef Colors_Watercolor_h
#define Colors_Watercolor_h

struct Watercolor {
    Watercolor() = default;

    //! the texture that makes up the "paper"
    ci::gl::TextureRef paper;
    
};

#endif
