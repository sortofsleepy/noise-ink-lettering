//
//  BlendComponent.h
//  Colors
//  Handle various blending functions. Adapted from oF 
//  Created by Joseph Chow on 1/6/16.
//
//

#ifndef Colors_BlendComponent_h
#define Colors_BlendComponent_h

enum BLENDMODE {
    BLENDMODE_SCREEN,
    BLENDMODE_ALPHA,
    BLENDMODE_ADD,
    BLENDMODE_MULTIPLY,
    BLENDMODE_SUBTRACT
};

struct BlendComponent {
    BLENDMODE currentMode;
    
    void enableBlend(BLENDMODE mode=BLENDMODE_SCREEN){
        glEnable(GL_BLEND);
        
        switch(mode){
            case BLENDMODE_SCREEN:
                glBlendEquation(GL_FUNC_ADD);
                glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE);
                break;
                
            case BLENDMODE_ALPHA:
                glBlendEquation(GL_FUNC_ADD);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                break;
                
            case BLENDMODE_ADD:
                glBlendEquation(GL_FUNC_ADD);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                break;
                
            case BLENDMODE_SUBTRACT:
                glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                break;
                
            case BLENDMODE_MULTIPLY:
                glBlendEquation(GL_FUNC_ADD);
                glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA /* GL_ZERO or GL_ONE_MINUS_SRC_ALPHA */);
                break;
        }
        
        currentMode = mode;
    }
    
    void disableBlend(){
        glDisable(GL_BLEND);
    }
    
};
#endif
