//
//  InkBuffer.h
//  Colors
//
//  Created by Joseph Chow on 1/6/16.
//
//

#ifndef Colors_InkBuffer_h
#define Colors_InkBuffer_h

#include "PingPongBuffer.h"
using namespace ci;
using namespace ci::app;
using namespace std;

struct InkBuffer {
    InkBuffer(){
        misc.allocate();
    }
    
    gl::TextureRef getOutput(){
        return misc.getOutput();
    }
    
    void begin(){
        misc.begin();
    }
    
    void end(){
        misc.end();
    }
    PingPongBuffer misc;
    

};

#endif
