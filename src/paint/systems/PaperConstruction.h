//
//  PaperConstruction.h
//  Colors
//  A system responsible for construcing the paper base.
//  Created by Joseph Chow on 1/5/16.
//
//

#ifndef __Colors__PaperConstruction__
#define __Colors__PaperConstruction__

#include <stdio.h>
#include "entityx/System.h"
#include "PingPongBuffer.h"
#include "PaperTextures.h"
#include "PaperCompositor.h"
#include "Watercolor.h"
#include "BlendComponent.h"
class PaperConstruction : public entityx::System<PaperConstruction>{
    
public:
    void update( entityx::EntityManager &entities, entityx::EventManager &events, entityx::TimeDelta dt ) override;
private:
    entityx::TimeDelta  previous_dt = 1.0 / 60.0;
};
#endif /* defined(__Colors__PaperConstruction__) */
