//
//  InkXAmtShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/30/15.
//
//

#ifndef WaterColors_InkXAmtShader_h
#define WaterColors_InkXAmtShader_h

#include "InkShader.h"

class InkXAmtShader : public InkShader {
public:
    InkXAmtShader(){
        loadShader("InkXAmt.glsl","shaders");
        
    }
    void update(int w, int h,
                vec2 pxSize,
                float f1, float f2, float f3,
                const gl::TextureRef& velDen,
                const gl::TextureRef& misc,
                const gl::TextureRef& flowInk,
                const gl::TextureRef& fixInk)
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(velDen,1);
        gl::ScopedTextureBind tex1(misc,2);
        gl::ScopedTextureBind tex2(flowInk,3);
        gl::ScopedTextureBind tex3(fixInk,4);
        
        
        
        shader->uniform("pxSize", pxSize);
        shader->uniform("FixRate", vec3(f1, f2, f3));
        shader->uniform("VelDenMap", 1);
        shader->uniform("MiscMap", 2);
        shader->uniform("FlowInkMap", 3);
        shader->uniform("FixInkMap",  4);
        drawPlane(w, h);
      
    }
};
#endif
