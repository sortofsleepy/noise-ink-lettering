//
//  GetVelDenShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_GetVelDenShader_h
#define WaterColors_GetVelDenShader_h


#include "InkShader.h"

class GetVelDenShader : public InkShader{
public:
    GetVelDenShader(){
        loadShader("GetVelDen.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float wf_mul,
                float evapor,
                const gl::TextureRef& misc,
                const gl::TextureRef& dist1,
                const gl::TextureRef& dist2)
    {
        
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(misc,0);
        gl::ScopedTextureBind tex1(dist1,1);
        gl::ScopedTextureBind tex2(dist2,2);
        //gl::ScopedTextureBind tex3(velDen,3);
        
        //shader->uniform("pxSize", pxSize);
        shader->uniform("wf_mul", wf_mul);
        shader->uniform("Evapor", evapor);
        shader->uniform("MiscMap",  0);
        shader->uniform("Dist1Map",  1);
        shader->uniform("Dist2Map",2);
        //shader->uniform("VelDenMap",  3);
        drawPlane(w, h);
        
    }
};

#endif
