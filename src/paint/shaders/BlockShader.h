//
//  BlockShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_BlockShader_h
#define WaterColors_BlockShader_h
#include "InkShader.h"

class BlockShader : public InkShader{
public:
    BlockShader(){
        loadShader("BlockShader.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float advect_p,
                float b11, float b12, float b13,
                float b21, float b22,
                float p1, float p2, float p3,
                float toe_p,
                float omega,
                vec2 offset,
                const gl::TextureRef& misc,
                const gl::TextureRef& velDen,
                const gl::TextureRef& flowInk,
                const gl::TextureRef& fixInk,
                const gl::TextureRef& disorder)
    {
       
        gl::ScopedGlslProg prog(shader);{
            //gl::ScopedTextureBind tex0(misc,0);
            gl::ScopedTextureBind tex1(velDen,1);
            gl::ScopedTextureBind tex2(flowInk,2);
            gl::ScopedTextureBind tex3(fixInk, 3);
            gl::ScopedTextureBind tex4(disorder,4);
            
            
            //shader->uniform("pxSize",pxSize);
            shader->uniform("A0", 0.4444444f);
            shader->uniform("advect_p", advect_p);
            shader->uniform("Blk_1", vec3(b11, b12, b13));
            shader->uniform("Blk_2", vec2(b21, b22));
            shader->uniform("Pin_w", vec3(p1, p2, p3));
            shader->uniform("toe_p", toe_p);
            shader->uniform("Omega", omega);
            shader->uniform("Corn_mul", pow(2.0f, 0.5f));
            shader->uniform("offset", offset);
            shader->uniform("MiscMap",  0);
            shader->uniform("VelDenMap",  1);
            shader->uniform("FlowInkMap", 2);
            shader->uniform("FixInkMap", 3);
            shader->uniform("DisorderMap",4);
            drawPlane(w, h);
            
        }
        
   
      

        
    }
};


#endif
