#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "math.h"
#include "LetterLoader.hpp"
#include "cinder/CameraUi.h"
#include "PSystem.hpp"
#include "Paint.h"
#include "PingPongBuffer.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class NoiseLetteringApp : public App {
public:
    void setup() override;
    void mouseDown( MouseEvent event ) override;
    void update() override;
    void draw() override;
    
    PSystem system;
    LetterLoader loader;
    Paint sim;
};

void NoiseLetteringApp::setup()
{
    loader = LetterLoader(true);
    loader.loadLetter("letters/Hello.jpg");
    
    std::vector<vec3> positions = loader.getPositions();
    
    system.setup(positions);
    sim.setupPaper("grain.jpg", "alum.jpg", "pinning.jpg");
    
}

void NoiseLetteringApp::mouseDown( MouseEvent event )
{
    sim.begin();
    gl::drawSolidCircle(event.getPos(), 30.0);
    sim.end();
}

void NoiseLetteringApp::update()
{
    system.update();
    sim.update();
    
    sim.begin();
    system.draw();
    sim.end();
   
}

void NoiseLetteringApp::draw()
{
    // clear the window to gray
    gl::clear( Color( 1,1,1) );
    sim.draw();
}

CINDER_APP( NoiseLetteringApp, RendererGl ,[] ( App::Settings *settings ) {
    settings->setWindowSize( 1280, 720 );
    settings->setMultiTouchEnabled( false );
} )
