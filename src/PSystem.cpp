//
//  PSystem.cpp
//  FlowLetters
//
//  Created by Joseph Chow on 5/3/16.
//
//

#include "cinder/Rand.h"
#include "PSystem.hpp"
using namespace ci;
using namespace ci::app;
using namespace std;

PSystem::PSystem(){}

void PSystem::setup(std::vector<ci::vec3> startingpositions){
    // Create initial particle layout.
    vector<Particle> particles;
    
    numParticles = startingpositions.size();
    //numParticles = 20000;
    app::console()<<numParticles<<"\n";
    
    gl::Fbo::Format ffmt;
    ffmt.setSamples(4);
    fbo = gl::Fbo::create(getWindowWidth(), getWindowHeight(),ffmt);
    
    ptexture = gl::Texture::create(loadImage(loadAsset("spark.png")));
    
    particles.assign( numParticles, Particle() );
    
    ci::Color colors[6] = {
        ci::normalize(Color(28,64,143)),
        ci::normalize(Color(13,48,66)),
        ci::normalize(Color(24,106,166)),
        ci::normalize(Color(23,134,156)),
        ci::normalize(Color(91,18,166)),
        ci::normalize(Color(156,22,132))
        
    };
 
    for(int i = 0; i < numParticles; ++i){
        auto &p = particles.at( i );
        
        //grab a random starting positions
        ci::vec3 pos = startingpositions.at(randInt(0,startingpositions.size()));
        ci::vec3 start;
        start.x = randFloat() * getWindowWidth();
        start.y = getWindowHeight() * randFloat();
        start.z = randFloat();
        pos += randFloat(-4,4) + randFloat(-4,4);
        
        p.pos = start;
        p.home = pos;
        p.color = colors[randInt(0,5)];
    }
    
    // Create particle buffers on GPU and copy data into the first buffer.
    // Mark as static since we only write from the CPU once.
    mParticleBuffer[mSourceIndex] = gl::Vbo::create( GL_ARRAY_BUFFER, particles.size() * sizeof(Particle), particles.data(), GL_STATIC_DRAW );
    mParticleBuffer[mDestinationIndex] = gl::Vbo::create( GL_ARRAY_BUFFER, particles.size() * sizeof(Particle), nullptr, GL_STATIC_DRAW );
    
    for( int i = 0; i < 2; ++i )
    {	// Describe the particle layout for OpenGL.
        mAttributes[i] = gl::Vao::create();
        gl::ScopedVao vao( mAttributes[i] );
        
        // Define attributes as offsets into the bound particle buffer
        gl::ScopedBuffer buffer( mParticleBuffer[i] );
        gl::enableVertexAttribArray( 0 );
        gl::enableVertexAttribArray( 1 );
        gl::enableVertexAttribArray( 2 );
        gl::vertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, pos) );
        gl::vertexAttribPointer( 1, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, color) );
        gl::vertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, home) );
    }
    
    
    gl::GlslProg::Format rfmt;
    rfmt.vertex(loadAsset("render-v.glsl"));
    rfmt.fragment(loadAsset("render-f.glsl"));
    
    mRenderProg = gl::GlslProg::create(rfmt);
    //mRenderProg = gl::getStockShader(gl::ShaderDef().color());
    
    gl::GlslProg::Format fmt;
    fmt.vertex(loadAsset("update.vs"))
    .feedbackFormat( GL_INTERLEAVED_ATTRIBS )
    .feedbackVaryings( { "position", "home", "color" } )
    .attribLocation( "iPosition", 0 )
    .attribLocation( "iColor", 1 )
    .attribLocation( "iHome", 2 );
    
    mUpdateProg = gl::GlslProg::create( fmt);
    
   

}
void PSystem::update(){
    // Update particles on the GPU
    gl::ScopedGlslProg prog( mUpdateProg );
    gl::ScopedState rasterizer( GL_RASTERIZER_DISCARD, true );	// turn off fragment stage
    mUpdateProg->uniform("noiseScale",200.0f);
    mUpdateProg->uniform("noiseStrength",8.0f);
    mUpdateProg->uniform("time", (float)getElapsedSeconds());
    
    // Bind the source data (Attributes refer to specific buffers).
    gl::ScopedVao source( mAttributes[mSourceIndex] );
    // Bind destination as buffer base.
    gl::bindBufferBase( GL_TRANSFORM_FEEDBACK_BUFFER, 0, mParticleBuffer[mDestinationIndex] );
    gl::beginTransformFeedback( GL_POINTS );
    
    // Draw source into destination, performing our vertex transformations.
    gl::drawArrays( GL_POINTS, 0, numParticles );
    
    gl::endTransformFeedback();
    
    // Swap source and destination for next loop
    std::swap( mSourceIndex, mDestinationIndex );
  
}

void PSystem::draw(){
    gl::clear(ColorA(0,0,0,0.8));
    gl::setMatricesWindowPersp( getWindowSize() );
    gl::enableDepthRead();
    gl::enableDepthWrite();
    
    gl::ScopedGlslProg render( mRenderProg );
    gl::ScopedTextureBind tex0(ptexture,0);
    mRenderProg->uniform("spark", 0);
    
    mRenderProg->uniform("time",(float)getElapsedSeconds());
    gl::ScopedVao vao( mAttributes[mSourceIndex] );
    gl::context()->setDefaultShaderVars();
    gl::drawArrays( GL_POINTS, 0, numParticles );
}