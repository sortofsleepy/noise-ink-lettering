//
//  PSystem.hpp
//  FlowLetters
//
//  Created by Joseph Chow on 5/3/16.
//
//

#ifndef PSystem_hpp
#define PSystem_hpp

#include <stdio.h>
#include "cinder/CameraUi.h"
struct Particle
{
    ci::vec3	pos;
    ci::vec3	home;
    ci::ColorA   color;

};


class PSystem {
    ci::gl::GlslProgRef mRenderProg;
    ci::gl::GlslProgRef mUpdateProg;
    
    // Descriptions of particle data layout.
    ci::gl::VaoRef		mAttributes[2];
    // Buffers holding raw particle data on GPU.
    ci::gl::VboRef		mParticleBuffer[2];
    
    // Current source and destination buffers for transform feedback.
    // Source and destination are swapped each frame after update.
    std::uint32_t	mSourceIndex		= 0;
    std::uint32_t	mDestinationIndex	= 1;

    ci::gl::FboRef fbo;
    ci::gl::TextureRef ptexture;
    int numParticles;
public:
    PSystem();
    void setup(std::vector<ci::vec3> startingpositions);
    void setup();
    void update();
    void draw();
};
#endif /* PSystem_hpp */
