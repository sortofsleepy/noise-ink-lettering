//
//  LetterLoader.cpp
//  FlowLetters
//
//  Created by Joseph Chow on 4/27/16.
//
//

#include "LetterLoader.hpp"
using namespace ci;
using namespace ci::app;

LetterLoader::LetterLoader(bool debug){
    this->debug = debug;
};

void LetterLoader::loadLetter(std::string path){
 
    
    image = Channel(loadImage(loadAsset(path)));
    positions = moca::ocv::findContours(image).points;
    
    //only init batch if we debug
    if(debug){
        batch = gl::VertBatch::create();
        
        batch->begin(GL_POINTS);
        for(int i = 0; i < positions.size();++i){
            batch->vertex(positions.at(i));
        }
        batch->end();
    }
    
}

//! TODO BSplines or is that overkill? meant to be short simple points
vector<vector<ci::vec3> > LetterLoader::generatePaths(int pathSize,int numPaths){
    
    //determine if we can actually generate the specified number of paths
    int totalPoints = pathSize * numPaths;
    
    if(totalPoints > positions.size()){
        CI_LOG_E("Unable to generate paths based on the settings - results in more points than available");
    }
    
    
    vector<vector<ci::vec3> > paths;
    for(int i = 0; i < numPaths;++i){
  
        vector<ci::vec3> path_points;
        
        //! starting point
        auto pt = randFloat(0,positions.size());
        
        //whether or not we go forward or backwards
        auto fwdback = randInt(1);
        
        //push back first point
        path_points.push_back(positions.at(pt));
        
    
        //pathSize-1 because we push back the first point above
        for(int a = 1; a < pathSize; ++a){
            
            // if zero we go backwards, otherwise go forwards
            if(fwdback == 0){
                path_points.push_back(positions.at(pt - a));
            }else{
                path_points.push_back(positions.at(pt + a));
            }
            
            
        }
        
     
        paths.push_back(path_points);
    }
    
    return paths;
}

gl::TextureRef LetterLoader::encodeToTexture(){
    //! TODO 100x100 is 10000 possible slots? Too little?
    SurfaceRef surf = Surface::create(100,100, false);
   
    int currentPos = 0;
    for(int i = 0; i < 100;++i){
        for(int j = 0; j < 100; ++j){
          
            if (currentPos < positions.size()) {
                auto pix = positions.at(currentPos);
                surf->setPixel(vec2(i,j), ColorA(pix.x,pix.y,pix.z,1.0));
                currentPos++;
            }else{
                surf->setPixel(vec2(i,j), ColorA(0,0,0,1));
            }
        }
    }
    return gl::Texture::create(*surf.get());
}

void LetterLoader::draw(){
    if(debug){
        auto tex = gl::Texture::create(image);
        
        gl::setMatricesWindow(getWindowSize());
        gl::draw(tex,Rectf(0,0,getWindowWidth(),getWindowHeight()));
    }
}

void LetterLoader::drawPoints(){
    if(debug){
        batch->draw();
    }
}