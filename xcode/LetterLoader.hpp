//
//  LetterLoader.hpp
//  FlowLetters
//
//  Created by Joseph Chow on 4/27/16.
//
//

#ifndef LetterLoader_hpp
#define LetterLoader_hpp

#include "Contours.hpp"
#include "cinder/Log.h"
#include "cinder/Rand.h"

class LetterLoader {

    std::vector<ci::vec3> positions;
    ci::Channel image;
    
    ci::gl::VertBatchRef batch;
    bool debug;
public:
    LetterLoader(bool debug=false);
    void loadLetter(std::string path);
    
    //! encodes edge positions onto a texture so they can be referenced
    //! in a shader. Returns ci::gl::Texture
    ci::gl::TextureRef encodeToTexture();
    
    //! generates random paths based on the letter form.
   vector<vector<ci::vec3> > generatePaths(int pathSize=3,int numPaths=10);
    
    std::vector<ci::vec3> getPositions(){
        return positions;
    }
    
    //! for debugging - outputs the letter so we can get a sense of what it might look like
    void draw();
    void drawPoints();
};
#endif /* LetterLoader_hpp */
