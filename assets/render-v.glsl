#version 150


uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;
uniform float time;
in vec4		ciPosition;
in vec2		ciTexCoord0;
in vec3		ciNormal;
in vec4		color;
in vec4     ciColor;

in vec4   iColor;
out vec2	TexCoord;
out vec3	Normal;
out vec4    vColor;

void main( void )
{
    gl_PointSize = 1.0;
    gl_Position	= ciModelViewProjection * ciPosition;
    TexCoord	= ciTexCoord0;
    vColor = iColor;
    Normal		= ciNormalMatrix * ciNormal;
}
