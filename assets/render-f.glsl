#version 150
uniform float time;
uniform sampler2D spark;
out vec4 glFragColor;
in vec4 vColor;
void main(){
    vec2 position = gl_PointCoord.xy;
    vec4 sparkTex = texture(spark,position);
    vec4 color = vColor;
    glFragColor =  vec4(position.x *sin(time),position.y * cos(time),sparkTex.x,1.0);
}