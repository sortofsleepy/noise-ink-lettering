#version 150 core


uniform vec2 u_resolution;
uniform float noiseStrength;
uniform float noiseScale;
uniform float time;

in vec3   iPosition;
in vec3   iHome;
in vec4   iColor;

out vec3  position;
out vec3  home;
out vec4 color;


#include "noise_common.glsl"
#include "noise3d.glsl"
#include "noise.glsl"

const float attractorMass = 200.0;
const float G = 2.0;

const float dt2 = 1.0 / (60.0 * 60.0);

void main()
{
    position =  iPosition;
    home =      iHome;
    color = iColor;
  
    vec3 n = vec3(position.x / noiseScale, position.y / noiseScale,1.0);
    float angle = snoise(n * noiseStrength) + sin(time);
    
    vec3 force = position - home;
    float d = length(force);
    d = clamp(d,5.0,25.0);
    force = normalize(force);
    float strength = (G * 0.0503 * attractorMass) / (d * d);
    
    
    
    force *= (angle * cos(angle) + strength * noiseStrength);
    position -= force + force;
    

    
}