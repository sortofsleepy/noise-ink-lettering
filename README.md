Noise-Ink-Lettering
====

Simple experiment testing out ink-deposition with typography.

__Bulding and Running__
(note : only tested on OSX)

* first, put this in a folder called `projects` under your user's `documents` folder (this can of course be changed within the Xcode project)
* make sure you download [Cinder's OpenCV 3 block](https://github.com/cinder/Cinder-OpenCV/tree/dev) and place it in a folder called `blocks` within the root of this repo
* should be all set!


Note
====
The Cinder setup has changed a bit. Note that `libcinder.a` is now found in `/lib/macosx/Debug/libcinder.a` 